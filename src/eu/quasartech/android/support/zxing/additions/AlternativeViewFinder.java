/*
 * SupportFramework LITE Android Library - Copyright (C) 2012-2015 Diego Guarrella
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.quasartech.android.support.zxing.additions;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import jim.h.common.android.lib.zxing.ViewfinderView;
import jim.h.common.android.lib.zxing.camera.CameraManager;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.NinePatchDrawable;
import android.os.Build;
import android.util.AttributeSet;

import com.google.zxing.ResultPoint;

import eu.quasartech.android.support.R;

/**
 * Experimental custom ViewFinder with alternative frame drawables and optional
 * laser effect
 * 
 * @author Diego Guarrella
 * @version 1.0 beta
 */

public class AlternativeViewFinder extends ViewfinderView {

	@SuppressLint("NewApi")
	public static Bitmap getMaskedBitmap(Resources res, Bitmap source, Bitmap mask, int x, int y) {
		BitmapFactory.Options options = new BitmapFactory.Options();
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			options.inMutable = true;
		}
		options.inPreferredConfig = Bitmap.Config.ARGB_8888;
		
		Bitmap bitmap;
		if (source.isMutable()) {
			bitmap = source;
		} else {
			bitmap = source.copy(Bitmap.Config.ARGB_8888, true);
			source.recycle();
		}
		bitmap.setHasAlpha(true);
		Canvas canvas = new Canvas(bitmap);
		
		Paint paint = new Paint();
		paint.setXfermode(new PorterDuffXfermode(Mode.DST_IN));
		canvas.drawBitmap(mask, 0, 0, paint);
		mask.recycle();
		return bitmap;
	}

	public static Bitmap getNinePatchBitmap(int id,int x, int y, Resources res){
        // id is a resource id for a valid ninepatch

        Bitmap bitmap = BitmapFactory.decodeResource(res, id);

        byte[] chunk = bitmap.getNinePatchChunk();
        NinePatchDrawable np_drawable = new NinePatchDrawable(bitmap, chunk, new Rect(), null);
        np_drawable.setBounds(0, 0,x, y);

        Bitmap output_bitmap = Bitmap.createBitmap(x, y, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output_bitmap);
        np_drawable.draw(canvas);

        return output_bitmap;
    }

	WeakReference<Resources> res;

	protected boolean hasLaserEffect = false;

	public AlternativeViewFinder(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public boolean HasLaserEffect() {
		return hasLaserEffect;
	}

	@Override
	public void onDraw(Canvas canvas) {
		if (isInEditMode())
			return;

		Rect frame = CameraManager.get().getFramingRect();
		if (frame == null) {
			return;
		}
		int width = canvas.getWidth();
		int height = canvas.getHeight();

		// Draw the exterior (i.e. outside the framing rect) darkened
		paint.setColor(resultBitmap != null ? resultColor : maskColor);
		canvas.drawRect(0, 0, width, frame.top, paint);
		canvas.drawRect(0, frame.top, frame.left, frame.bottom + 1, paint);
		canvas.drawRect(frame.right + 1, frame.top, width, frame.bottom + 1,
				paint);
		canvas.drawRect(0, frame.bottom + 1, width, height, paint);

		if (resultBitmap != null) {
			// Draw the opaque result bitmap over the scanning rectangle
			paint.setAlpha(CURRENT_POINT_OPACITY);
			
			canvas.drawBitmap(resultBitmap, null, new Rect(frame.left+5,frame.top+5, frame.right-5, frame.bottom-5), paint);
			// canvas.drawBitmap(resultBitmap, null, frame, paint);
			// You can change original image here and draw anything you want to
			// be masked on it.
	
			NinePatchDrawable bg = (NinePatchDrawable) getResources()
					.getDrawable(R.drawable.camera_target_over_detected);
			if (bg != null) {
				bg.setBounds(frame.left, frame.top, frame.right + 1,
						frame.bottom + 1);
				bg.draw(canvas);
			}

		} else {

			NinePatchDrawable bg = (NinePatchDrawable) getResources()
					.getDrawable(R.drawable.camera_target_over_scan);
			if (bg != null) {
				bg.setBounds(frame.left, frame.top, frame.right + 1,
						frame.bottom + 1);
				bg.draw(canvas);
			}

			// Draw a red "laser scanner" line through the middle to show
			// decoding is active
			if (hasLaserEffect) {
				paint.setColor(laserColor);
				paint.setAlpha(SCANNER_ALPHA[scannerAlpha]);
				scannerAlpha = (scannerAlpha + 1) % SCANNER_ALPHA.length;
				int middle = frame.height() / 2 + frame.top;
				canvas.drawRect(frame.left + 2, middle - 1, frame.right - 1,
						middle + 2, paint);
			}

			Rect previewFrame = CameraManager.get().getFramingRectInPreview();
			float scaleX = frame.width() / (float) previewFrame.width();
			float scaleY = frame.height() / (float) previewFrame.height();

			List<ResultPoint> currentPossible = possibleResultPoints;
			List<ResultPoint> currentLast = lastPossibleResultPoints;
			if (currentPossible.isEmpty()) {
				lastPossibleResultPoints = null;
			} else {
				possibleResultPoints = new ArrayList<ResultPoint>(5);
				lastPossibleResultPoints = currentPossible;
				paint.setAlpha(CURRENT_POINT_OPACITY);
				paint.setColor(resultPointColor);
				synchronized (currentPossible) {
					for (ResultPoint point : currentPossible) {
						canvas.drawCircle(frame.left
								+ (int) (point.getX() * scaleX), frame.top
								+ (int) (point.getY() * scaleY), 6.0f, paint);
					}
				}
			}
			if (currentLast != null) {
				paint.setAlpha(CURRENT_POINT_OPACITY / 2);
				paint.setColor(resultPointColor);
				synchronized (currentLast) {
					for (ResultPoint point : currentLast) {
						canvas.drawCircle(frame.left
								+ (int) (point.getX() * scaleX), frame.top
								+ (int) (point.getY() * scaleY), 3.0f, paint);
					}
				}
			}

			// Request another update at the animation interval, but only
			// repaint the laser line,
			// not the entire viewfinder mask.
			postInvalidateDelayed(ANIMATION_DELAY, frame.left, frame.top,
					frame.right, frame.bottom);
		}
	}

	public void setHasLaserEffect(boolean hasLaserEffect) {
		this.hasLaserEffect = hasLaserEffect;
	}
}
