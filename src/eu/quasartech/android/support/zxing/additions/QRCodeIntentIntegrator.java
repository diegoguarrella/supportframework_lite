package eu.quasartech.android.support.zxing.additions;

import jim.h.common.android.lib.zxing.Intents;
import jim.h.common.android.lib.zxing.config.ZXingLibConfig;
import android.app.Activity;
import android.content.Intent;

/**
 * @author Jim.H
 */
public final class QRCodeIntentIntegrator {

	/**
	 * start to scan with default formats and character set.
	 * 
	 * @param activity
	 * @see #initiateScan(Activity, String, String)
	 */
	public static void initiateScan(Activity activity) {
		initiateScan(activity, null, null, null);
	}

	/**
	 * @param activity
	 *            parent activity
	 * @param customScannerActivity
	 * @param scanFormatsString
	 *            barcodeFormats separated by comma. Null value is accepted.<br />
	 *            <ul>
	 *            <li><strong>AZTEC</strong> Aztec 2D barcode format.</li>
	 *            <li><strong>CODABAR</strong> CODABAR 1D format.</li>
	 *            <li><strong>CODE_39</strong> Code 39 1D format.</li>
	 *            <li><strong>CODE_93</strong> Code 93 1D format.</li>
	 *            <li><strong>CODE_128</strong> Code 128 1D format.</li>
	 *            <li><strong>DATA_MATRIX</strong> Data Matrix 2D barcode
	 *            format.</li>
	 *            <li><strong>EAN_8</strong> EAN-8 1D format.</li>
	 *            <li><strong>EAN_13</strong> EAN-13 1D format.</li>
	 *            <li><strong>ITF</strong> ITF (Interleaved Two of Five) 1D
	 *            format.</li>
	 *            <li><strong>PDF_417</strong> PDF417 format.</li>
	 *            <li><strong>QR_CODE</strong> QR Code 2D barcode format.</li>
	 *            <li><strong>RSS_14</strong> RSS 14</li>
	 *            <li><strong>RSS_EXPANDED</strong> RSS EXPANDED</li>
	 *            <li><strong>UPC_A</strong> UPC-A 1D format.</li>
	 *            <li><strong>UPC_E</strong> UPC-E 1D format.</li>
	 *            <li><strong>UPC_EAN_EXTENSION</strong> UPC/EAN extension
	 *            format. Not a stand-alone format.</li>
	 *            </ul>
	 * @param characterSet
	 *            e.g. "utf-8"... or null
	 * @param config
	 *            can be null
	 * @see com.google.zxing.BarcodeFormat
	 */
	public static void initiateScan(Activity activity,
			Class<?> customScanActivity, String scanFormatsString, String characterSet, ZXingLibConfig config) {
		Intent intent = new Intent(activity, customScanActivity==null ? QRCodeScanActivity.class : customScanActivity);
		intent.putExtra(Intents.Scan.FORMATS, scanFormatsString);
		intent.putExtra(Intents.Scan.CHARACTER_SET, characterSet);
		intent.putExtra(ZXingLibConfig.INTENT_KEY, config);
		activity.startActivityForResult(intent, REQUEST_CODE);
	}
	
	/**
	 * @param activity
	 *            parent activity
	 * @param scanFormatsString
	 *            barcodeFormats separated by comma. Null value is accepted.<br />
	 *            <ul>
	 *            <li><strong>AZTEC</strong> Aztec 2D barcode format.</li>
	 *            <li><strong>CODABAR</strong> CODABAR 1D format.</li>
	 *            <li><strong>CODE_39</strong> Code 39 1D format.</li>
	 *            <li><strong>CODE_93</strong> Code 93 1D format.</li>
	 *            <li><strong>CODE_128</strong> Code 128 1D format.</li>
	 *            <li><strong>DATA_MATRIX</strong> Data Matrix 2D barcode
	 *            format.</li>
	 *            <li><strong>EAN_8</strong> EAN-8 1D format.</li>
	 *            <li><strong>EAN_13</strong> EAN-13 1D format.</li>
	 *            <li><strong>ITF</strong> ITF (Interleaved Two of Five) 1D
	 *            format.</li>
	 *            <li><strong>PDF_417</strong> PDF417 format.</li>
	 *            <li><strong>QR_CODE</strong> QR Code 2D barcode format.</li>
	 *            <li><strong>RSS_14</strong> RSS 14</li>
	 *            <li><strong>RSS_EXPANDED</strong> RSS EXPANDED</li>
	 *            <li><strong>UPC_A</strong> UPC-A 1D format.</li>
	 *            <li><strong>UPC_E</strong> UPC-E 1D format.</li>
	 *            <li><strong>UPC_EAN_EXTENSION</strong> UPC/EAN extension
	 *            format. Not a stand-alone format.</li>
	 *            </ul>
	 * @param characterSet
	 *            e.g. "utf-8"... or null
	 * @param config
	 *            can be null
	 * @see com.google.zxing.BarcodeFormat
	 */
	public static void initiateScan(Activity activity,
			String scanFormatsString, String characterSet, ZXingLibConfig config) {
		Intent intent = new Intent(activity, QRCodeScanActivity.class);
		intent.putExtra(Intents.Scan.FORMATS, scanFormatsString);
		intent.putExtra(Intents.Scan.CHARACTER_SET, characterSet);
		intent.putExtra(ZXingLibConfig.INTENT_KEY, config);
		activity.startActivityForResult(intent, REQUEST_CODE);
	}

	/**
	 * start to scan with default formats and character set.
	 * 
	 * @param activity
	 * @param config
	 *            can be null
	 * @see #initiateScan(Activity, String, String)
	 */
	public static void initiateScan(Activity activity, ZXingLibConfig config) {
		initiateScan(activity, "QR_CODE", null, config);
	}

	public static QRCodeIntentResult parseActivityResult(int requestCode,
			int resultCode, Intent intent) {
		if (requestCode == REQUEST_CODE) {
			if (resultCode == android.app.Activity.RESULT_OK) {
				String contents = intent.getStringExtra(Intents.Scan.RESULT);
				String formatName = intent
						.getStringExtra(Intents.Scan.RESULT_FORMAT);
				return new QRCodeIntentResult(contents, formatName);
			} else {
				return new QRCodeIntentResult(null, null);
			}
		}
		return null;
	}

	public static final int REQUEST_CODE = 0x0000c0de; // Only use bottom 16
														// bits

	private QRCodeIntentIntegrator() {
	}
}
