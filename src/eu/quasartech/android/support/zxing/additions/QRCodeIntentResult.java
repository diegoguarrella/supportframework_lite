package eu.quasartech.android.support.zxing.additions;

/**
 * @author Jim.H
 */
public final class QRCodeIntentResult {

	private final String contents;
	private final String formatName;

	QRCodeIntentResult(String contents, String formatName) {
		this.contents = contents;
		this.formatName = formatName;
	}

	public String getContents() {
		return contents;
	}

	public String getFormatName() {
		return formatName;
	}

	@Override
	public String toString() {
		return "QRCodeIntentResult [contents=" + contents + ", formatName="
				+ formatName + "]";
	}
}
