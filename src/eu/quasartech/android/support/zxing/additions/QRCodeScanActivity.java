/*
 * SupportFramework LITE Android Library - Copyright (C) 2012-2015 Diego Guarrella
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.quasartech.android.support.zxing.additions;

import java.util.List;

import jim.h.common.android.lib.zxing.CaptureActivity;
import jim.h.common.android.lib.zxing.camera.CameraManager;
import android.annotation.SuppressLint;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.hardware.Camera.Parameters;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;
import eu.quasartech.android.support.R;

/**
 * 
 * @author Diego Guarrella
 * 
 */
public class QRCodeScanActivity extends CaptureActivity {

	boolean flaslightOn = false;
	boolean hasFlashlight = false;
	final Handler mHandler = new Handler();
	MenuItem mItemFlashlight;
	private boolean preventUpdates;
	int tipCounter = 0;
	String tips[];

	TextView tvStatus;
	
	protected int actLayout = R.layout.act_qrscanner;

	@SuppressLint("InlinedApi")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		super.onCreate(savedInstanceState);
		
		Window window = getWindow();
		window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		this.supportRequestWindowFeature(Window.FEATURE_ACTION_BAR_OVERLAY);

		setContentView(actLayout);
		
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }		
		
		ActionBar aBar = getSupportActionBar();
		aBar.setHomeButtonEnabled(true);
		aBar.setTitle(R.string.act_title_qrscanner);
		aBar.setSubtitle(R.string.act_subtitle);
		aBar.setBackgroundDrawable(getResources().getDrawable(
				R.drawable.actionbar_overlay_bg));
		aBar.setDisplayHomeAsUpEnabled(true);

		hasFlashlight = getPackageManager().hasSystemFeature(
				PackageManager.FEATURE_CAMERA_FLASH);

		onCreateStep2();

		tvStatus = (TextView) findViewById(R.id.tvStatus);

		tips = getResources().getStringArray(R.array.tips);
		tvStatus.setText(tips[0]);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (hasFlashlight) {
			MenuInflater inflater = getMenuInflater();
			inflater.inflate(R.menu.qrscanner, menu);
		}

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int itemId = item.getItemId();
		
		if (itemId == android.R.id.home) {
			finish();
			return true;
		} else if (itemId == R.id.action_flashlight) {
			switchFlashlight();
			return true;
		} else {
			return super.onOptionsItemSelected(item);
		}

	}

	@Override
	protected void onPause() {
		super.onPause();
		preventUpdates = true;
	}

	@Override
	protected void onResume() {
		super.onResume();
		preventUpdates = false;

		mHandler.postDelayed(new Runnable() {
			@Override
			public void run() {
				tipCounter = (++tipCounter) % tips.length;
				if (!preventUpdates) {
					tvStatus.setText(tips[tipCounter]);
					mHandler.postDelayed(this, 4000L);
				}
			}
		}, 4000L);
	}

	protected void switchFlashlight() {
		if (hasFlashlight) {
			CameraManager cManager = CameraManager.get();
			Parameters params = cManager.camera.getParameters();

			List<String> flashModes = params.getSupportedFlashModes();

			if (flashModes == null || flashModes.size() == 0) {
				return;
			} else {
				String flashMode = params.getFlashMode();

				if (!Parameters.FLASH_MODE_TORCH.equals(flashMode)) {

					if (flashModes.contains(Parameters.FLASH_MODE_TORCH)) {
						params.setFlashMode(Parameters.FLASH_MODE_TORCH);
						cManager.camera.setParameters(params);
					} else {
						params.setFlashMode(Parameters.FLASH_MODE_ON);
						cManager.camera.setParameters(params);
					}

					flaslightOn = true;
					Toast.makeText(this,
							this.getString(R.string.flashlight_on),
							Toast.LENGTH_SHORT).show();
				} else {
					flaslightOn = false;
					params.setFlashMode(Parameters.FLASH_MODE_OFF);
					cManager.camera.setParameters(params);
					Toast.makeText(this,
							this.getString(R.string.flashlight_off),
							Toast.LENGTH_SHORT).show();
				}
			}
		}
	}
}
