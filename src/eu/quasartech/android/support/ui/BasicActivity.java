/*
 * SupportFramework LITE Android Library - Copyright (C) 2012-2015 Diego Guarrella
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.quasartech.android.support.ui;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;

public abstract class BasicActivity extends AppCompatActivity
		implements Handler.Callback, OnClickListener {

	public enum ORIENTATION {
		LANDSCAPE, PORTRAIT
	}

	public static final long DOUBLE_CLICK_TIME_DELTA = 600;// milliseconds
	public long lastClickTime = 0;
	private ColorDrawable aBarBackgroundColor;
	

	protected Handler mHandler;
	private ORIENTATION screenOrientation = ORIENTATION.PORTRAIT;

	protected ActionBar aBar;

	@Override
	protected void attachBaseContext(Context newBase) {
		super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
	}

	protected int getAlphaforActionBar(int scrollY) {
		int minDist = 0,maxDist = 650;
		if(scrollY>maxDist){ 
			return 255;
			}
		else if(scrollY<minDist){
			return 0;
			}
		else {
			int alpha = 0;
			alpha = (int)  ((255.0/maxDist)*scrollY);
			return alpha;
		}
	}

	public Handler getHandler() {
		if (mHandler == null)
			mHandler = new Handler();
		return mHandler;
	}

	@Override
	public boolean handleMessage(Message inputMessage) {
		return false;
	}

	protected abstract View makeMenuView(Bundle savedInstanceState);
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.v("SupportFramework LITE Android Library",
				"(c)2012-2015 Diego Guarrella - Released under Apache 2.0 free software license");
	}
	
	public void setActionBarBackgroundAlpha(int alpha) {
		if (aBarBackgroundColor != null)
			aBarBackgroundColor.setAlpha(alpha);
	}

	public void setActionBarBackgroundColor(int resId) {
		aBarBackgroundColor = new ColorDrawable(this.getResources().getColor(
				resId));
		this.getSupportActionBar().setBackgroundDrawable(aBarBackgroundColor);
	}
	
	public void switchOrientation(ORIENTATION o) {
		WindowManager.LayoutParams attrs = getWindow().getAttributes();

		screenOrientation = o;

		if (o == ORIENTATION.PORTRAIT) {
			getSupportActionBar().show();
			attrs.flags &= (~WindowManager.LayoutParams.FLAG_FULLSCREEN);
			getWindow().setAttributes(attrs);
		} else {
			getSupportActionBar().hide();
			attrs.flags |= WindowManager.LayoutParams.FLAG_FULLSCREEN;
			getWindow().setAttributes(attrs);
		}
	}
}