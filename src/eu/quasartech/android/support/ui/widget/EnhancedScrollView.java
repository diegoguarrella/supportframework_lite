/*
 * SupportFramework LITE Android Library - Copyright (C) 2012-2015 Diego Guarrella
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.quasartech.android.support.ui.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ScrollView;

public class EnhancedScrollView extends ScrollView {

	public interface ScrollViewListener {
		void onScrollChanged(EnhancedScrollView scrollView, int x, int y,
				int oldx, int oldy);
	}
	
	public View headerView;

	private ScrollViewListener scrollViewListener = null;
	private int lastTopValueAssigned=0;

	public EnhancedScrollView(Context context) {
		super(context);
	}

	public EnhancedScrollView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public EnhancedScrollView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	protected void onScrollChanged(int l, int t, int oldl, int oldt) {
		super.onScrollChanged(l, t, oldl, oldt);

		if (scrollViewListener != null) {
			scrollViewListener.onScrollChanged(this, l, t, oldl, oldt);
		}
		
		if (headerView!=null && Build.VERSION.SDK_INT >=11)
			parallaxImage(headerView);
	}

	@SuppressLint("NewApi")
	private void parallaxImage(View view) {
	    Rect rect = new Rect();
	    view.getLocalVisibleRect(rect);
	    if (lastTopValueAssigned != rect.top){
	        lastTopValueAssigned = rect.top;
	        view.setY((float) (rect.top/2.0));
	    }
	}
	
	public void setScrollViewListener(ScrollViewListener scrollViewListener) {
		this.scrollViewListener = scrollViewListener;
	}
}