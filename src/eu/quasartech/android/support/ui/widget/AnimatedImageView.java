/*
 * SupportFramework LITE Android Library - Copyright (C) 2012-2015 Diego Guarrella
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.quasartech.android.support.ui.widget;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

public class AnimatedImageView extends ImageView {

	/**
	 * Instantiates a new AnimatedImageView.
	 *
	 * @param context
	 *            the context
	 */
	public AnimatedImageView(Context context) {
		super(context);
	}

	/**
	 * Instantiates a new AnimatedImageView.
	 *
	 * @param context
	 *            the context
	 * @param attrs
	 *            the attrs
	 */
	public AnimatedImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	/**
	 * Instantiates a new AnimatedImageView with style.
	 *
	 * @param context
	 *            the context
	 * @param attrs
	 *            the attrs
	 * @param defStyle
	 *            the def style
	 */
	public AnimatedImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	protected void onVisibilityChanged(View changedView, int visibility) {
		super.onVisibilityChanged(changedView, visibility);
		updateAnimationsState();
	}

	@Override
	public void onWindowFocusChanged(boolean hasWindowFocus) {
		super.onWindowFocusChanged(hasWindowFocus);
		updateAnimationsState();
	}

	private void updateAnimationsState() {
		boolean running = getVisibility() == View.VISIBLE && hasWindowFocus();
		updateAnimationState(getDrawable(), running);
		updateAnimationState(getBackground(), running);
	}

	private void updateAnimationState(Drawable drawable, boolean running) {
		if (drawable instanceof AnimationDrawable) {
			AnimationDrawable animationDrawable = (AnimationDrawable) drawable;
			if (running) {
				animationDrawable.start();
			} else {
				animationDrawable.stop();
			}
		}
	}
}